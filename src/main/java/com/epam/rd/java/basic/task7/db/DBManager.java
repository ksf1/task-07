package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private Connection connection;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			try {
				instance = new DBManager();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	private DBManager() throws SQLException {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("app.properties"));
			connection = DriverManager.getConnection(properties.getProperty("connection.url"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	public List<User> findAllUsers() throws DBException {

		List<User> userList = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from users");
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				String login = resultSet.getString("login");
				int id = resultSet.getInt("id");
				userList.add(new User(login, id));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}


		return userList;
	}

	public boolean insertUser(User user) throws DBException {

		String userName = user.getLogin();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(
					"INSERT INTO users (login) values (?)", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, userName);
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()){
				user.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {

		for (User user : users) {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("DELETE from users WHERE login = ?");
				preparedStatement.setString(1, user.getLogin());
				preparedStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}


		return false;
	}

	public User getUser(String login) throws DBException {
		int id = 0;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(
					"SELECT id from users WHERE login = ?");
			preparedStatement.setString(1, login);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				id = resultSet.getInt("id");
			}
		} catch (SQLException e){
			e.printStackTrace();
		}

		return new User(login, id);
	}

	public Team getTeam(String name) throws DBException {
		int id = 0;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT id from teams WHERE name = ?");
			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				id = resultSet.getInt("id");
			}
		} catch (SQLException e){
			e.printStackTrace();
		}

		return new Team(name, id);
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teamList = new ArrayList<>();

		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * from teams");
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				String name = resultSet.getString("name");
				int id = resultSet.getInt("id");
				teamList.add(new Team(name, id));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}

		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		String teamName = team.getName();

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(
					"INSERT INTO teams (name) values (?)", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, teamName);
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()){
				team.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		try {
			connection.setAutoCommit(false);
			for (Team team : teams){
				PreparedStatement preparedStatement = connection.prepareStatement(
						"INSERT INTO users_teams (user_id, team_id) values (?, ?)");
				preparedStatement.setInt(1, user.getId());
				preparedStatement.setInt(2, team.getId());
				preparedStatement.execute();
			}
			connection.commit();

		} catch (SQLException e) {
			rollback(connection);
			throw new DBException("1", e);

	} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> listTeams = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(
					"SELECT * from teams as t1 join " +
							"(SELECT team_id from users_teams WHERE user_id = ?) as t2 ON t1.id = t2.team_id");
			preparedStatement.setInt(1, user.getId());
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				listTeams.add(Team.createTeam(resultSet.getString("name")));
			}
		} catch (SQLException e){
			e.printStackTrace();
		}

		return listTeams;
	}


	public boolean deleteTeam(Team team) throws DBException {
		boolean b = false;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE from teams WHERE name = ?");
			preparedStatement.setString(1, team.getName());
			b = preparedStatement.execute();
		} catch (SQLException e){
			e.printStackTrace();;
		}

		return b;
	}

	public boolean updateTeam(Team team) throws DBException {
		boolean b = false;
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("UPDATE teams SET name = ? WHERE id = ?");
			preparedStatement.setString(1, team.getName());
			preparedStatement.setInt(2, team.getId());
			b = preparedStatement.execute();
		} catch (SQLException e){
			e.printStackTrace();;
		}

		return b;
	}

	public static void rollback(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
